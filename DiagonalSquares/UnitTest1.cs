﻿using System;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DiagonalSquares
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Test0()
        {
            RunTest("input00.txt", "output00.txt");
        }

        [TestMethod]
        public void Test1()
        {
            RunTest("input01.txt", "output01.txt");
        }

        [TestMethod]
        public void Test2()
        {
            RunTest("input02.txt", "output02.txt");
        }

        [TestMethod]
        public void Test3()
        {
            RunTest("input03.txt", "output03.txt");
        }

        [TestMethod]
        public void Test4()
        {
            RunTest("input04.txt", "output04.txt");
        }

        [TestMethod]
        public void Test5()
        {
            RunTest("input05.txt", "output05.txt");
        }

        public void RunTest(string inputFile, string expectedFile)
        {
            var ms = new MemoryStream();
            var oldOut = Console.Out;

            using (var tr = new StreamReader(inputFile))
            using (var tw = new StreamWriter(ms, Encoding.ASCII, 4096, true))
            {
                Console.SetIn(tr);
                Console.SetOut(tw);
                try
                {
                    Solution.Main(new string[0]);
                }
                catch (Exception e)
                {
                    Assert.Fail(e.Message);
                }
            }

            ms.Position = 0;
            int row = 0;
            Console.SetOut(oldOut);

            using (var tr = new StreamReader(expectedFile))
            using (var tra = new StreamReader(ms))
            {
                while (!tr.EndOfStream && !tra.EndOfStream)
                {
                    decimal expected = decimal.Parse(tr.ReadLine());

                    var a = tra.ReadLine();
                    Console.WriteLine(a);

                    decimal actual = decimal.Parse(a);

                    var diff = Math.Abs(expected - actual);
                    if (diff > 0.0001m)
                    {
                        Assert.Fail("Failed row #{0}. Expected {1}, was {2}. Diff is {3}", row, expected, actual, diff);
                    }
                    row++;
                }

                if (tr.EndOfStream != tra.EndOfStream)
                {
                    Assert.Fail("Different size of output and expected streams");
                }
            }
        }
    }
}
